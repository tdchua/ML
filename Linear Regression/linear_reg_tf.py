#Attempt to learn tensorflow
#Univariate Linear Regression
#Reference: https://www.tensorflow.org/programmers_guide/low_level_intro

import numpy as np
import tensorflow as tf
import pandas as pd
import os

if __name__ == "__main__":


    #This reads the file coursera_1.txt which contains the feature vectors, and the label vector
    dataset = pd.read_csv("coursera_1.txt", sep = ',', header = None)
    dataset = np.array(dataset)

    if(dataset.shape[1] == 2):
        print("Univariate Linear Regression")

    else:
        print("Multivariate Linear Regression")

    #Parsing the X and the Y for features and labels respectively
    x = dataset[:, 0:dataset.shape[1] - 1]
    x = tf.constant(x, dtype=tf.float32)

    y = dataset[:, dataset.shape[1] - 1]
    y = np.reshape(y, (x.shape[0], 1))
    y_true = tf.constant(y, dtype=tf.float32)

    #Instantiating a model for linear regression
    linear_model = tf.layers.Dense(units = 1)

    #The prediction made by the model
    y_pred = linear_model(x)

    #Using MSE as the loss/cost function for training
    loss = tf.losses.mean_squared_error(labels=y_true, predictions=y_pred)

    #The optimizing function would be gradient descent
    optimizer = tf.train.GradientDescentOptimizer(0.01)
    train = optimizer.minimize(loss)

    #to instantiate all the variables
    init = tf.global_variables_initializer()
    with tf.variable_scope("dense", reuse=tf.AUTO_REUSE):
        weights = tf.get_variable("kernel")

    #the session is for the runtime for the training of the model
    sess = tf.Session()
    sess.run(init)

    #Iterated over 2000 times to decrease the loss at every iteration so that the weights would converge to the minimum
    for i in range(2000):
      _, loss_value = sess.run((train, loss))
      print(loss_value, "Weights: ", sess.run(weights))


    writer = tf.summary.FileWriter('.')
    writer.add_graph(tf.get_default_graph())
