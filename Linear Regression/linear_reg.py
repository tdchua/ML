#this project was inspired by Andrew Ng's Machine Learning Coursera courseself.
#As the filename suggests, the project revolves around machine learning through the use of linear regression.

import numpy as np
import pandas as pd

def compute_cost(x, y, theta):
    #Uses mean squared error to determine cost
    hypo = np.dot(x,theta)

    cost = sum(np.power(hypo - y, 2))
    cost = (1 / (2 * x.shape[0])) * cost
    return cost

def train_linear_reg(x, y, theta, alpha):
    #training the model uses gradient descent
    m = x.shape[0]
    for a in range(1, 1500):
        hypo = np.dot(x, theta)
        error = hypo - y
        inside_summation = np.transpose(x).dot(error)
        deriv_cost = inside_summation * alpha * (1 / m)
        theta = theta - deriv_cost

    print("Trained Hypothesis")
    return theta


def normalize_features(x):
    std_dev_features = np.std(x, axis = 0, dtype = 'f')
    mean_features = np.mean(x, axis = 0, dtype = 'f')

    for a in range(1, x.shape[1]):
        x[:, a] = np.subtract(x[:, a], mean_features[a])
        x[:, a] = np.divide(x[:, a], std_dev_features[a])

    return x, std_dev_features, mean_features
#





if __name__ == "__main__":
    dataset = pd.read_csv("airfoil_self_noise.csv", sep = ',', header = None)
    dataset = np.array(dataset)
    if(dataset.shape[1] == 2):
        print("Univariate Linear Regression")

    else:
        print("Multivariate Linear Regression")


    print("Dataset")
    print("Size: ", dataset.shape)

    #Parsing the file
    x = dataset[:, 0:dataset.shape[1] - 1]

    # add one column of ones so that we can perform dot multiplication easily
    x = np.hstack((np.ones((x.shape[0], 1), dtype = 'f'), x))
    y = dataset[:, dataset.shape[1] - 1]
    y = np.reshape(y, (x.shape[0], 1))

    #Initializing the theta/weight values with 0
    theta = np.zeros((x.shape[1] + 1, 1))

    #this step is important for features that were initially in different orders of magnitutde
    print("Feature Normalized")
    x, std_dev_features, mean_features = normalize_features(x)

    #Cost is measured with the Mean-Squared-Error equation
    print("Computing Cost")
    print(compute_cost(x, y, theta))

    #Training is done by updating the weights using gradient descent
    print("Training")
    theta = train_linear_reg(x, y, theta, 0.01)
    print(theta)

    print("Predicting")
    input_feat = np.ones((1, x.shape[1]))
    print("Please input features in ascending order x1,x2,x3...")
    for a in range(1, x.shape[1]):
        input_feat[0, a] = float(input())
        input_feat[0, a] = np.subtract(input_feat[0, a], mean_features[a])
        input_feat[0, a] = np.divide(input_feat[0, a], std_dev_features[a])


    prediction = np.dot(input_feat,theta)
    print("Prediction: ", prediction)
